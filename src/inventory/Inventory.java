
package inventory;

import static inventory.Item.inventory;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ashley Kretschmar
 */
public class Inventory {
    private String name;
    private List<Items> items;
    public static Item[] inventory = new Item[100];
    private int itemCounter = 0; // counts numbers of items in the inventory.
    private int itemQuantity = 0;
    
    public Inventory(String name, List<Items> items){
        this.name = name;
        this.items = items;
    }
    public List<Items> getItems(){
        return items;
    }
    public void addItem(Item newItem) {
        inventory[itemCounter] = newItem;
        itemCounter++;
        itemQuantity++;
    }
    public void printInventory() {
        for (int i = 0; i < itemCounter; i++) {
            System.out.println("ID: " + inventory[i].itemID
                    + "\t Name: " + inventory[i].getItemName())
                    + "\t Quantity:" + inventory[i].getItemQuantity());
        }
    }
}
